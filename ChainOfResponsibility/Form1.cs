﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChainOfResponsibility
{
    public partial class Form1 : Form
    {
        EinsUndEinsGratisChain chain1 = new EinsUndEinsGratisChain();
        ProzentProduktgruppe chain2 = new ProzentProduktgruppe();
        MitarbeiterRabatt chain3 = new MitarbeiterRabatt();

        public Form1()
        {
            InitializeComponent();

            chain1.SetNextChain(chain2);
            chain2.SetNextChain(chain3);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        List<Product> products = new List<Product>();

        private void button1_Click(object sender, EventArgs e)
        {
            products.Add(new Product() { BarCode = "12345", Bezeichnung = "Cola", Preis = 0.69m, Zusatz = string.Empty });
            ResolveProducts();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            products.Add(new Product() { BarCode = "98765", Bezeichnung = "Caffee", Preis = 6.99m, Zusatz = string.Empty });
            ResolveProducts();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            products.Add(new Product() { BarCode = "54321", Bezeichnung = "Lachs", Preis = 8.49m, Zusatz = string.Empty });
            ResolveProducts();
        }
        
        private void button4_Click(object sender, EventArgs e)
        {
            products.Add(new Product() { BarCode = "8878787878787887", Bezeichnung = "Pizza", Preis = 4.19m, Zusatz = string.Empty });
            ResolveProducts();
        }

        private void ResolveProducts()
        {
            chain1.Check(products);
            AddToList(products.Last());
        }

        private void AddToList(Product product)
        {
            listBox1.Items.Add(product.ToString());
            if (!string.IsNullOrEmpty(product.Zusatz))
            {
                listBox1.Items.Add(product.Zusatz);
            }

            label1.Text = "Summe: " + products.Sum(x => x.Preis);
        }
    }
}
