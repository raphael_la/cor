﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    class Product
    {
        public string BarCode { get; set; }
        public string Bezeichnung { get; set; }
        public string Zusatz { get; set; }
        public decimal Preis { get; set; }

        public override string ToString()
        {
            return $"{Bezeichnung.PadRight(14)} {Preis.ToString().PadLeft(6)}";
        }
    }
}
