﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    class EinsUndEinsGratisChain : IKassaChain
    {
        IKassaChain next;
        public void Check(IEnumerable products)
        {
            var gratisBarcode = "12345";
            var scannedItems = products as List<Product>;
            var lastScannedItem = scannedItems.Last();
            if (lastScannedItem.BarCode.Equals(gratisBarcode) && scannedItems.Where(x => x.BarCode.Equals(gratisBarcode)).Count() %2 == 0)
            {
                lastScannedItem.Zusatz = "1+1 Gratis";
                lastScannedItem.Preis = 0.00m;
                return;
            }

            if (next != null)
                next.Check(products);
        }

        public void SetNextChain(IKassaChain next)
        {
            this.next = next;
        }
    }

    class ProzentProduktgruppe : IKassaChain
    {
        IKassaChain next;
        public void Check(IEnumerable products)
        {
            var rabBarCodes = new string[] { "98765", "98764" };
            var scannedItems = products as List<Product>;
            var lastScannedItem = scannedItems.Last();
            if (rabBarCodes.Contains(lastScannedItem.BarCode))
            {
                lastScannedItem.Zusatz = "25% PG Rabatt";
                var newPrice = lastScannedItem.Preis * 0.75m;
                lastScannedItem.Preis = decimal.Round(newPrice, 2);
                return;
            }

            if (next != null)
                next.Check(products);
        }

        public void SetNextChain(IKassaChain next)
        {
            this.next = next;
        }
    }

    
    class MitarbeiterRabatt : IKassaChain
    {
        IKassaChain next;
        public void Check(IEnumerable products)
        {
            var rabBarCodes = new string[] { "54321", "54320" };
            var scannedItems = products as List<Product>;
            var lastScannedItem = scannedItems.Last();
            if (rabBarCodes.Contains(lastScannedItem.BarCode))
            {
                lastScannedItem.Zusatz = "Mitarbeiter Rabatt";
                var newPrice = lastScannedItem.Preis * 0.75m;
                lastScannedItem.Preis = decimal.Round(newPrice, 2);
                return;
            }

            if (next != null)
                next.Check(products);
        }

        public void SetNextChain(IKassaChain next)
        {
            this.next = next;
        }
    }

}
