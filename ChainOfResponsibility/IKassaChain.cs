﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    interface IKassaChain
    {
        void Check(IEnumerable products);
        void SetNextChain(IKassaChain next);
    }
}
